---
title: "Data Science Assessment Exercise"
author: "Cennen Del Rosario"
output:
  pdf_document: default
  html_document: default
---

# 0. Initial setup

The following libraries, setup and constants are used althrough out the code.

$\hspace{5 mm}$

```{r initialization, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson=""}

library(dplyr)
library(tidyverse)
library(kableExtra)
library(ggplot2)
library(fastDummies)
library(MASS)
library(e1071)
library(randomForest)

DIRECTORY_FOLDER <- "/Users/ken/Documents/Applications/Teradata"
setwd(DIRECTORY_FOLDER)

SEED_NUMBER <- 12345
```


# 1. Data Loading


```{r data_loading, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="initialization"}

# data loading
raw.data <- read.csv("credit_data_track2_part_2_1.csv", stringsAsFactors = FALSE)
all.variables <- setdiff(names(raw.data), c("X","gender"))
all.variables
```

$\hspace{5 mm}$

The data that was given appears to have 21 independent variables (instead of 20) and 1 target variable called "accepted". At first glance, columns "X" and "gender" seem redundant. For the column gender, we can quickly check that it is by applying the following code. The matching yield no rows, indicating that the gender part of the "personal_status" is the same as in the "gender" field.

$\hspace{5 mm}$

```{r verify_redundant_gender, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_loading"}

# verify dependency of gender from personal_status variables
separate(raw.data, "personal_status", into = c("status_gender","others"), sep = "_") %>%
  dplyr::select("status_gender","gender") %>%
    filter(status_gender != gender)
```

# 2. Data Exploration

Next we go to data exploration. The following functions are useful when getting some information about each input variable.

$\hspace{5 mm}$

```{r data_exploration, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_loading"}

# utility functions for data exploration
plot.table <- function(column){
  count.values <- table(column)
  count.values <- as.matrix(count.values)
  count.values <- round(100*count.values/sum(count.values), digits = 2)
  count.values[,1] <- paste(count.values, "%", sep = "")
  knitr::kable(count.values) %>%
    kable_styling(latex_options = "striped", position = "left")
}

plot.summary <- function(summary){
  values <- as.data.frame(summary) %>%
              dplyr::select(Freq) %>%
                separate(col = "Freq", sep = ":", into = c("Stat", "Value"))
  knitr::kable(values) %>%
    kable_styling(latex_options = "striped", position = "left") %>%
      row_spec(0, bold = TRUE)
}

plot.histo <- function(column, name){
  hist(column, col = c("gold", "gold1", "gold2", "gold3"), main = "", xlab = name)
}

plot.bar <- function(column){
  count.values <- as.data.frame(table(column))
  names(count.values)[1] <- "Value"
  par(mfrow=c(1,2))
  p <- ggplot(data = count.values, aes(x = Value, y = Freq)) +
          geom_bar(stat="identity", fill="steelblue") +
          theme_minimal()
  
  p
}

```


###Variable 1: checking_status

```{r data_exploration_1, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: checking_status
plot.table(raw.data["checking_status"])
```

At first sight, the categorical values above can be decomposed into dummy variables and assigned with 0 or 1.


###Variable 2: duration

```{r data_exploration_2, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: duration
plot.bar(raw.data["duration"])
```

###Variable 3: credit_history

```{r data_exploration_3, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: credit_history
plot.table(raw.data["credit_history"])
```

Similarly, values above can also be translated into 0 or 1. Also, it can be observed that most of the applicants have existing credits.


###Variable 4: purpose

```{r data_exploration_4, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: purpose
plot.table(raw.data["purpose"])
```

Here, it can be observed that most of the loaners are spending (as they state) to material things (gadgets, car, furniture, etc.) when applying for loan. For the analysis, these values can be decomposed into dummy variables and assigned with 0 or 1 later on.

###Variable 5: credit_amount

```{r data_exploration_5, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: credit_amount
plot.summary(summary(raw.data["credit_amount"]))
plot.histo(raw.data["credit_amount"][[1]], name = "Credit amount")
```

Missing values are handled appropriately.

###Variable 6: savings_status

```{r data_exploration_6, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: savings_status
plot.table(raw.data["savings_status"])
```

Here, categorical values can be with assigned with 0 or 1. Alternatively, each category can be represented by a numeric value (e.g. mean of the group).

###Variable 7: employment

```{r data_exploration_7, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: employment
plot.table(raw.data["employment"])
```

Similarly, each category here can be represented by a numeric value (e.g. group mean, 0, or 1).

###Variable 8: installment_commitment

```{r data_exploration_8, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: installment_commitment
plot.bar(raw.data["installment_commitment"])
```

It can be observed that applicants tend to stretch out their loan terms to maximum. 

###Variable 9: personal_status

```{r data_exploration_9, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: personal_status
plot.table(raw.data["personal_status"])
```

Here, gender is better to be extracted into a separate column. On the other hand, a separate "civil status" variable should be created. This will take the values: single, married, separated, divorced, and widowed, which in turn can be translated into dummy variables. It good to note that seldomly, maried, divorced and widowed men apply for loans. 

###Variable 10: other_parties

```{r data_exploration_10, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: other_parties
plot.table(raw.data["other_parties"])
```

Categorical values here can also assigned with 0 or 1 for analysis later. Or can be considered for ignoring or regrouping because more than 90% of the data  has no other parties involved.

###Variable 11: residence_since

```{r data_exploration_11, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: residence_since
plot.bar(raw.data["residence_since"])
```

###Variable 12: property_magnitude

```{r data_exploration_12, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: property_magnitude
plot.table(raw.data["property_magnitude"])
```

###Variable 13: age

```{r data_exploration_13, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: age
plot.summary(summary(raw.data["age"]))
plot.histo(raw.data["age"][[1]], name = "Age")
```

###Variable 14: asnm

```{r data_exploration_14, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: asnm
plot.summary(summary(raw.data["asnm"]))
plot.histo(raw.data["asnm"][[1]], name = "ASNM")
```

###Variable 15: other_payment_plans

```{r data_exploration_15, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: other_payment_plans
plot.table(raw.data["other_payment_plans"])
```

Categorical values can be also be assigned with 0 or 1.

###Variable 16: housing

```{r data_exploration_16, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: housing
plot.table(raw.data["housing"])
```

Categorical values here can be also decomposed into dummy variables.

###Variable 17: existing_credits

```{r data_exploration_17, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: existing_credits
plot.bar(raw.data["existing_credits"])
```

###Variable 18: job

```{r data_exploration_18, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: job
plot.table(raw.data["job"])
```

Categorical values can be assigned with 0 or 1. There is no need to wrangle it because no clear and repeated pattern can be observed in the values.

###Variable 19: num_dependents

```{r data_exploration_19, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: num_dependents
plot.bar(raw.data["num_dependents"])
```

###Variable 20: own_telephone

```{r data_exploration_20, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: own_telephone
plot.table(raw.data["own_telephone"])
```

Values can be re-assigned with 0 or 1.

###Variable 21: foreign_worker

```{r data_exploration_21, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_exploration"}
# variable: foreign_worker
plot.table(raw.data["foreign_worker"])
```

Values here can be translated into 0 or 1.

# 3. Data Wrangling

Basically, categorical variables will be separated into dummy variable (with one left unassigned to avoid multicollinearity). The target is to obtain two sets of data sets: one with pure dummy encoded variables and another one where bins are replaced by representive values. It has been decided this way to see if there is any effect when representative values are used in the modeling.

$\hspace{5 mm}$

```{r data_wrangling, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_loading"}

# impute NAs
row.with.na <- which(is.na(raw.data["credit_amount"]))
if(length(row.with.na) != 0){
  raw.data$credit_amount[row.with.na] <- mean(raw.data$credit_amount[-row.with.na])
}

# wrangle multiple values
civil.status <- raw.data["personal_status"] %>% 
                  separate(col = "personal_status", sep = "_", 
                           into = c("personal.gender","civil.status")) %>%
                    dplyr::select(civil.status)

unique(civil.status)
raw.data <- cbind(raw.data, civil.status) %>%
              mutate("cs.single" = ifelse(civil.status == 'single', 1, 0),
                     "cs.divorced.separated" = ifelse(grepl('(.)*divorced/separated(.)*', 
                                                            civil.status), 1, 0),
                    "cs.married" = ifelse(grepl('(.)*married(.)*', civil.status), 1, 0),
                    "cs.widowed" = ifelse(grepl('(.)*widowed', civil.status), 1, 0))
```

```{r data_wrangling_1, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling"}

# dummy coding
sparse.data <- raw.data
sparse.data <- dummy_cols(sparse.data, remove_first_dummy = TRUE,
            select_columns = c("checking_status", "credit_history", "purpose", 
                               "savings_status", "employment", 
                               "other_parties", "property_magnitude",
                               "other_payment_plans", "housing", "job")) %>%
              mutate("own_telephone" = ifelse(own_telephone == 'none', 0, 1),
                      "foreign_worker" = ifelse(foreign_worker == 'no', 0, 1), 
                      "gender" = ifelse(gender == 'male', 1, 0))
```

```{r data_wrangling_2, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling_1"}

sparse.cols <- setdiff(names(sparse.data), c("X", "checking_status", "credit_history", 
                                             "purpose", "savings_status", "employment",
                                             "other_parties",
                                             "property_magnitude", "personal_status",
                                             "other_payment_plans", "housing", "job", 
                                             "civil.status", "cs.widowed"))

```



```{r data_wrangling_3, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling"}

# convert numeric bins to representative numbers
converted.sparse.data <- raw.data
converted.sparse.data <- dummy_cols(raw.data, remove_first_dummy = TRUE,
                  select_columns = c("checking_status", "credit_history", 
                                     "purpose", "other_parties", 
                                     "property_magnitude", "employment", 
                                     "savings_status", "other_payment_plans", 
                                     "housing", "job")) %>%
                    mutate("own_telephone" = ifelse(own_telephone == 'none', 0, 1),
                            "foreign_worker" = ifelse(foreign_worker == 'no', 0, 1), 
                            "gender" = ifelse(gender == 'male', 1, 0))
converted.sparse.data <- converted.sparse.data %>%
      mutate("savings_status" = ifelse(savings_status == '1000DM', 1200,
                                  ifelse(savings_status == '500_to_1000DM', 750,
                                    ifelse(savings_status == '100_to_500DM', 300,
                                      ifelse(savings_status == "100DM", 50, 0)))))

converted.sparse.data <- converted.sparse.data %>%
      mutate("employment" = ifelse(employment == ">7yrs", 9,
                              ifelse(employment == "4_to_7yrs", 6.5,
                                ifelse(employment == "1_to_4yrs", 2.5,
                                  ifelse(employment == "<1yr", 0.5, 0)))))

```


```{r data_wrangling_4, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling_3"}
# convert numeric bins to a number
converted.cols <- setdiff(names(converted.sparse.data), 
                          c("X", "checking_status", "credit_history", "purpose", 
                            "personal_status", "other_parties", "property_magnitude",
                            "personal_status", "other_payment_plans", "housing", 
                            "job", "civil.status", "cs.widowed", "employment_1_to_4yrs",
                            "employment_4_to_7yrs","employment_unemployed",
                            "employment_<1yr", "savings_status_<100DM",
                            "savings_status_500_to_1000DM",
                            "savings_status_>1000DM", "savings_status_100_to_500DM"))
```


$\hspace{5 mm}$

Below is the code that will generate the two data sets mentioned aboved.

$\hspace{5 mm}$

```{r data_wrangling_5, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling_4"}

# Set A: training and test data
n.observations <- nrow(raw.data)
set.seed(SEED_NUMBER)
rows <- sample(1:n.observations, 0.8*n.observations)
training.a <- sparse.data[rows, sparse.cols]
test.a <- sparse.data[-rows, sparse.cols]

# Set B: training and test data
training.b <- converted.sparse.data[rows, converted.cols]
test.b <- converted.sparse.data[-rows, converted.cols]

```


# 4. Predictive modeling
## 4.a Logistic Regression

The glm package of R and stepAIC are used to speed up the feature selection in logistic regression. The stepwise selection: forward, backward and both directions, are all considered. Furthermore, variables resulting from these algorithms are checked for significant. The level of signifance used in here is $\alpha = 0.05$.

$\hspace{5 mm}$

```{r data_modeling_logistic_init, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling_5"}

# utility functions
compute.error.rate <- function(model, test.data){
  probabilities <- predict(model, newdata = test.data, type = "response")
  predicted <- ifelse(probabilities > 0.5, 1, 0)
  1 - mean(predicted == test.data$accepted)
}
```

#### Set A: Data pure of dummy variables

```{r data_modeling_logistic_a, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, results="hide", cache=TRUE, dependson="data_modeling_logistic_init"}

# fitting via bidirectional
logistic.both.model <- glm(accepted ~., data = training.a, family = binomial) %>%
                          stepAIC(direction = "both", trace = FALSE)
summary(logistic.both.model)

logistic.both.reduced.model <- glm(accepted ~ duration + installment_commitment + 
    asnm + foreign_worker + cs.divorced.separated + 
    checking_status_0_to_200DM + checking_status_None + `checking_status_>200DM` + 
    credit_history_Existing_credits_paid_till_now + 
    credit_history_No_credits_taken_or_all_paid + credit_history_All_credits_paid_duly + 
    purpose_education + purpose_new_car + 
    `savings_status_<100DM` + savings_status_100_to_500DM + employment_4_to_7yrs + 
    other_parties_guarantor + other_payment_plans_bank + other_payment_plans_stores + 
    housing_rent, family = binomial, data = training.a)
summary(logistic.both.reduced.model)


# fitting via forward direction
logistic.forward.model <- glm(accepted ~ ., data = training.a, family = binomial)  %>%
                          stepAIC(direction = "forward", trace = FALSE)
summary(logistic.forward.model)

logistic.forward.reduced.model <- glm(accepted ~ duration + installment_commitment +
    foreign_worker +  checking_status_0_to_200DM + checking_status_None +
    `checking_status_>200DM` + credit_history_Existing_credits_paid_till_now + 
    credit_history_No_credits_taken_or_all_paid + credit_history_All_credits_paid_duly +
    purpose_education + purpose_new_car + `savings_status_<100DM` + 
    savings_status_100_to_500DM + employment_4_to_7yrs +
    other_parties_guarantor + other_payment_plans_bank, 
    data = training.a, family = binomial)
summary(logistic.forward.reduced.model)


# fitting via backward direction
logistic.backward.model <- glm(accepted ~ ., data = training.a, family = binomial) %>%
                          stepAIC(direction = "backward", trace = FALSE)
summary(logistic.backward.model)

logistic.backward.reduced.model <- glm(accepted ~ duration + installment_commitment + 
    asnm + foreign_worker + cs.divorced.separated + 
    checking_status_0_to_200DM + checking_status_None + `checking_status_>200DM` + 
    credit_history_Existing_credits_paid_till_now + 
    credit_history_No_credits_taken_or_all_paid + credit_history_All_credits_paid_duly + 
    purpose_education + purpose_new_car + 
    `savings_status_<100DM` + savings_status_100_to_500DM + employment_4_to_7yrs + 
    other_parties_guarantor + other_payment_plans_bank + other_payment_plans_stores + 
    housing_rent,
    data = training.a, family = binomial)
summary(logistic.backward.reduced.model)


# collecting results
log.models.a <- list(logistic.both.model, logistic.forward.model, 
                       logistic.backward.model, logistic.both.reduced.model, 
                       logistic.forward.reduced.model, 
                      logistic.backward.reduced.model)
error.rates.a <- matrix(sapply(log.models.a, function(m){ compute.error.rate(m, test.a) }),
                      nrow = 3, ncol = 2)
```

#### Set B: Mixture of dummy variables and representative values

```{r data_modeling_logistic_b, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, results="hide", cache=TRUE, dependson="data_modeling_logistic_a"}

# fitting via bidirectional
logistic.both.model <- glm(accepted ~., data = training.b, family = binomial) %>%
                          stepAIC(direction = "both", trace = FALSE)
summary(logistic.both.model)

logistic.both.reduced.model <- glm(accepted ~ duration + 
     installment_commitment + asnm + foreign_worker + cs.divorced.separated +
     checking_status_0_to_200DM + 
     checking_status_None + `checking_status_>200DM` + 
     credit_history_Existing_credits_paid_till_now + 
     credit_history_No_credits_taken_or_all_paid + 
     credit_history_All_credits_paid_duly + purpose_education + 
     purpose_new_car + other_parties_guarantor + 
     other_payment_plans_bank + other_payment_plans_stores + housing_rent, 
    family = binomial, data = training.b)
summary(logistic.both.reduced.model)


# fitting via forward direction
logistic.forward.model <- glm(accepted ~ ., data = training.b, family = binomial)  %>%
                          stepAIC(direction = "forward", trace = FALSE)
summary(logistic.forward.model)
#compute.error.rate(logistic.forward.model, test.b)

logistic.forward.reduced.model <- glm(accepted ~ duration + installment_commitment + 
    asnm +foreign_worker + checking_status_0_to_200DM + checking_status_None + 
    `checking_status_>200DM` + credit_history_Existing_credits_paid_till_now + 
    credit_history_No_credits_taken_or_all_paid + 
    credit_history_All_credits_paid_duly + purpose_new_car + 
    other_parties_guarantor + 
    other_payment_plans_bank + other_payment_plans_stores, 
    data = training.b,
    family = binomial)
summary(logistic.forward.reduced.model)


# fitting via backward direction
logistic.backward.model <- glm(accepted ~ ., data = training.b, family = binomial) %>%
                          stepAIC(direction = "backward", trace = FALSE)
summary(logistic.backward.model)

logistic.backward.reduced.model <- glm(accepted ~ duration + installment_commitment + 
    asnm + foreign_worker + cs.divorced.separated + checking_status_0_to_200DM + 
    checking_status_None + `checking_status_>200DM` +
    credit_history_Existing_credits_paid_till_now +
    credit_history_No_credits_taken_or_all_paid +
    credit_history_All_credits_paid_duly + purpose_education + 
    purpose_new_car + other_parties_guarantor + 
    other_payment_plans_bank + other_payment_plans_stores,
    data = training.b, family = binomial)
summary(logistic.backward.reduced.model)

# collecting results
log.models.b <- list(logistic.both.model, logistic.forward.model, 
                       logistic.backward.model, logistic.both.reduced.model, 
                       logistic.forward.reduced.model, 
                      logistic.backward.reduced.model)
error.rates.b <- matrix(sapply(log.models.b, function(m){ compute.error.rate(m, test.b) }),
                      nrow = 3, ncol = 2)
```

```{r data_modeling_logistic_result, echo=FALSE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_logistic_b"}
result <- cbind(error.rates.a, error.rates.b)
rownames(result) <- c("Bi-directional", "Forward", "Backward")
colnames(result) <- c("Algorithm", "Reduced", "Algorithm", "Reduced")

kable(result, caption = "Error rates obtained through logistic regression") %>%
  kable_styling("striped") %>%
   column_spec(1, bold = TRUE) %>%
    add_header_above(c(" ", "Set A" = 2, "Set B" = 2))
```

**Best logistic regression model:**  Using the Set A data, the forward selection model has been analyzed further to get rid of variables that have no sufficient proof of being significant. The test error rate in this case is 25.5%.

$\hspace{5 mm}$

## 4.b Support Vector Machines

First, the feature variables need to be scaled in order to improve the performance of the SVM optimizer.

```{r data_modeling_svm_init, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling_5"}
scale.features <- function(test.data){
  matrix.data <- as.matrix(test.data)
  target.variable <- matrix.data[,"accepted"]
  scaled.data <- scale(matrix.data, center = TRUE, scale = TRUE)
  scaled.data <- data.frame(scaled.data)
  scaled.data$accepted <- target.variable
  scaled.data$accepted <- as.factor(scaled.data$accepted)
  scaled.data
}

scaled.train.a <- scale.features(training.a)
scaled.train.b <- scale.features(training.b)
scaled.test.a <- scale.features(test.a)
scaled.test.b <- scale.features(test.b)
```

$\hspace{5 mm}$

Then, two variants of the model will be considered: one using the linear kernel, and the other one with radial kernel. Both model types are trained under the two sets of data. 

```{r data_modeling_svm_1, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_svm_init"}

# fitting with linear kernel
set.seed(SEED_NUMBER)
svm.tune.linear.a <- tune(svm, accepted ~ ., data = scaled.train.a, kernel = "linear",
                       ranges = list(cost = c(0.01, 0.1, 1, 5, 8, 10, 12, 20, 30, 50)))
summary(svm.tune.linear.a)
```

$\hspace{5 mm}$

```{r data_modeling_svm_2, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_svm_1"}

# fitting with linear kernel
set.seed(SEED_NUMBER)
svm.tune.linear.b <- tune(svm, accepted ~ ., data = scaled.train.b, kernel = "linear",
                       ranges = list(cost = c(0.01, 0.1, 1, 5, 8, 10, 50, 100)))
summary(svm.tune.linear.b)
```

$\hspace{5 mm}$

```{r data_modeling_svm_3, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_svm_2"}

# fitting with radial kernel
set.seed(SEED_NUMBER)
svm.tune.radial.a <- tune(svm, accepted ~ ., data = scaled.train.a, kernel = "radial",
                       ranges = list(cost = c(100, 150, 180, 200, 220, 250, 300),
                                     gamma = c(01e-05, 0.0001, 0.001, 0.01, 0.1, 1)))
summary(svm.tune.radial.a)
```

$\hspace{5 mm}$

```{r data_modeling_svm_4, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_svm_3"}

# fitting with radial kernel
set.seed(SEED_NUMBER)
svm.tune.radial.b <- tune(svm, accepted ~ ., data = scaled.train.b, kernel = "radial",
                       ranges = list(cost = c(100, 150, 180, 200, 220, 250, 300),
                                     gamma = c(01e-05, 0.0001, 0.001, 0.01, 0.1, 1)))
summary(svm.tune.radial.b)
```

$\hspace{5 mm}$

The results are summarized in the table below.

```{r data_modeling_svm_n, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_svm_4"}

# utility function
error.rate.svm <- function(svm.model, test.data){
  predicted <- predict(svm.model, newdata = test.data)
  1 - mean(predicted == test.data$accepted)
}

# get the best fit models
result <- matrix(c(svm.tune.linear.a$best.performance,
                 svm.tune.radial.a$best.performance,
                 error.rate.svm(svm.tune.linear.a$best.model, scaled.test.a),
                 error.rate.svm(svm.tune.radial.a$best.model, scaled.test.a),
                 svm.tune.linear.b$best.performance,
                 svm.tune.radial.b$best.performance,
                 error.rate.svm(svm.tune.linear.b$best.model, scaled.test.b),
                 error.rate.svm(svm.tune.radial.b$best.model, scaled.test.b)),
                 nrow = 2, ncol = 4)
rownames(result) <- c("Linear Kernel", "Radial Kernel")
colnames(result) <- c("Training", "Test", "Training", "Test")

kable(result, caption = "Error rates obtained through SVM") %>%
  kable_styling("striped") %>%
   column_spec(1, bold = TRUE) %>%
    add_header_above(c(" ", "Set A" = 2, "Set B" = 2))
```

**Best SVM model:** The model that is based on radial kernel performed very well. It has parameters: cost = 200 or 300, and gamma =  0.0001, regardless of which data set it is trained to. It yielded a test error rate of 26%.


## 4.c Random Forest

For the final machine learning technique, unwrangled variables are preferred. Random Forest works by establishing a solid tree structure, so categorical values are more favorable this time.

$\hspace{5 mm}$

```{r data_modeling_rf_init, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_wrangling_5"}

# wrangled data set
var.names <- setdiff(names(raw.data), c("X", "personal_status", 
                           "cs.single", "cs.divorced.separated",
                           "cs.married", "cs.widowed"))
rf.data <- as.data.frame(unclass(raw.data[var.names]))
training.rf <- rf.data[rows,]
test.rf <- rf.data[-rows,]

feature.size <- length(var.names) - 1
p.range <- floor(sqrt(feature.size)/2):feature.size
```


```{r data_modeling_rf_1, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_rf_init"}
# wrangled data
error.rates <- sapply(p.range, function(p) {
  set.seed(SEED_NUMBER)
  rand.model <- randomForest(accepted ~ ., data = training.rf, 
                           mtry = p, ntree = 1000,importance = TRUE)

  probabilities <- predict(rand.model, test.rf, type = "response")
  predicted <- ifelse(probabilities > 0.5, 1, 0)
  1 - mean(predicted == test.rf$accepted)
})

```


```{r data_modeling_rf_2, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_rf_1"}

result <- data.frame("Features" = p.range, "Error rate" = error.rates)
best.rf <- randomForest(accepted ~ ., data = training.rf, 
                           mtry = 12, ntree = 1000, importance = TRUE)

kable(result, caption = "Error rates obtained through Random Forest") %>%
  kable_styling("striped") %>%
   column_spec(1, bold = TRUE)
```

$\hspace{5 mm}$

**Best Random Forest model:** The one that has p = 12 (i.e. 12 feature variables are considered at random in each split/branch made by the random forest) and 1000 ensemble trees (repetitions).

# 5. Model Evaluation

We summarize the best performing models below.

```{r data_final_modeling, echo=TRUE, message=FALSE, warning=FALSE, include=TRUE, cache=TRUE, dependson="data_modeling_rf_2"}

summary.result <- matrix(c("Logistic Regression", "Reduced forward search result", "25.5%", 
         "full dummy encoded variables",
         "Support Vector Machine",	"Radial kernel", "26%",
         "either set but Set A is better",
         "Random Forest", "12 variables per split", "20%",
         "no variable encoding required"),
          nrow = 3, ncol = 4, byrow = TRUE)
colnames(summary.result) <- c("Technique", "Set-up", "Error rate", "Data set used")

kable(summary.result, caption = "Summary of best performing models") %>%
  kable_styling("striped") %>%
   row_spec(c(0,3), bold = TRUE)
```

$\hspace{5 mm}$

It can be seen that the random forest model, which was obtained by limiting to 12 the number of random predictors per decision split, has the most superior performance. It is 80% accurate in predicting whether the applicant will be accepted or not in the loan. Hence, it will be my preferred model.

Unfortunately, it is hard to identify how the feature variables affect the prediction because Random Forest is a type of black-box modeling. We cannot interpret the effect of each variable. This came as a good trade off for having the highest chance of correctly identifying whether an applicant will be rejected or not in his loan application in future.